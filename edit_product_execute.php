<?php
	include("cfg/bootstrap.php");
	
	if (!isset($_POST["product"])) header("Location: search.php?edit=false");
	
	if ($db->queryArrayParams("UPDATE cs_product SET Name = ?, Description = ?, ProductCategory = ? WHERE id = ?",
			array('ssii'),array(&$_POST["product"], &$_POST["product_desc"], &$_POST["category_id"], &$_POST["id"])) == '') {
		die($db->error());
	}
	
	//Retriving the dynamic name generated for each record corresponding to the product in cs_stock_adjustment.
	$post_keys = array_values(array_keys($_POST));
	$instock_id = array();
	foreach($post_keys as $key) {
		if(strpos($key, 'stock_') !== false) {
			array_push($instock_id, explode("stock_", $key)[1]);
		}
	} 

	foreach($instock_id as $sid) {
		if ($db->queryArrayParams("UPDATE cs_stock_adjustment SET Value = ? WHERE ID = ?",
				array('ii'), array(&$_POST["stock_". $sid], &$sid)) == '') {
			die($db->error());
		}
	}
	
	$date_time = date('Y-m-d h:i:s');
	
	if ($_POST["new_adjust"] != "empty" AND $_POST["adjustment_val"] != "") {
		
		if ($db->queryArrayParams("INSERT INTO cs_stock_adjustment VALUES(NULL,?,?,?,?)",
				array('isis'), array(&$_POST["id"], &$_POST["new_adjust"],
					 &$_POST["adjustment_val"], &$date_time)) == '') {
					 	echo'noooo';
			die($db->error());
		}
	}
	header("Location: search.php?edit=true");
	exit;
?>
	