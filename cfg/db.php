<?php
// This code is from Amir Saboury (amir.saboury.net/)
class DB {
	var $host = '';
	var $lastQuery='';
	var $user = '';
	var $password = '';
	var $database = '';
	var $persistent = false;
	var $conn = NULL;
	var $result = false;
	function DB($host, $user, $password, $database, $persistent = false) {
		$this->host = $host;
		$this->user = $user;
		$this->password = $password;
		$this->database = $database;
		$this->persistent = $persistent;
	}
	function open() {
		$this->conn = new mysqli($this->host, $this->user, $this->password, $this->database);
		if ($this->conn->connect_errno) {
			echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
		}	
		return true;
	}
	function close() {
		return mysqli_close($this->conn);
	}
	function error() {
		return (mysqli_errno($this->conn) . "<br />" . $this->lastQuery);
	}
	//$param should be all referenced in an array
	function queryNoPrams($sql) {
		$this->lastQuery = $sql;
		$stmt = '';
		if ($stmt = $this->conn->prepare($sql)) {
			$stmt->execute();
		}
		return $stmt;
	}
	//To exacute query from client to server, in order to protect from injection, prepare has been used.
	//Recieves an array of refrences to bind them with the statement and then excute them
	function queryArrayParams($sql, $types = array(), $by_ref_params_array = array()) {
		$this->lastQuery = $sql;
		$stmt = '';
		if ($stmt = $this->conn->prepare($sql)) {
			call_user_func_array(array(&$stmt, "bind_param"), array_merge($types, $by_ref_params_array));
			$stmt->execute();
		}
		return $stmt;
	}
	function fetchAssocWithBind($stmt, $by_ref_array) {
		call_user_func_array(array($stmt, "bind_result"), $by_ref_array);
		// returns a copy of a value
		$copy = create_function('$param', 'return $param;');
		
		$results = array();
		while ($stmt->fetch()) {
			// array_map will preserve keys when done here and this way
			$results[] = array_map($copy, $by_ref_array);
		}
		return $results;
	}
	function closeStatement($stmt) {
		$stmt->close();
	}
	//In case the query is just made in server with available data, query will be used
	function query($sql) {
		$this->lastQuery = $sql;
		return $this->conn->query($sql);
	}
	//
	function closeResult($res) {
		$res->close();
	}
	
	function affectedRows() {
		return mysqli_affected_rows($this->conn);
	}
	function fetchObject($res) {
		return $res->fetch_object(MYSQLI_ASSOC);
	}
	function fetchArray($res) {
		return $res->fetch_array(MYSQLI_NUM);
	}
	function fetchAssoc($res) {
		return mysqli_fetch_assoc($res);
	}
	function freeResult($res) {
		return mysqli_free_result($res);
	}
}

?>