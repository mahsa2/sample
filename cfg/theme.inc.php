<?php 
	class Theme {
		var $list="";
		var $file;
		var $content;
		var $BLOCKS;
		function Theme($theme_file) {
			$this->file = $theme_file;
			$this->f = fopen($this->file,"r");
			$this->content = fread($this->f,filesize($this->file));
			fclose($this->f);
		}
		function general($tags,$data) {
			$this->content = str_replace($tags,$data,$this->content);
		}
		function newBlock($to,$tags) {
			$__1 = explode("<!-- " . $to . " -->",$this->content);
			$__2 = explode("<!-- /" . $to . " -->",$__1[1]);
			$___1 = $__1[0];
			$___2 = $__2[0];
			$___3 = $__2[1];
			$____1 = $___1 . "<!-- " . md5($___2) . " -->" . $___3;
			$this->list[] = $to;
			$this->BLOCKS[$to]["p"] = $___2;
			$this->BLOCKS[$to]["tag"] = md5($___2);
			$this->BLOCKS[$to]["tags"] = $tags;
			$this->BLOCKS[$to]["block"] = "";
			$this->content = $____1;
		}
		function removeBlock($to) {
			$this->content = $this->remove($to,$this->content);
		}
		function remove($it,$fromIt) {
			//echo $it;
			//echo $fromIt;
			$__1 = explode("<!-- " . $it . " -->",$fromIt);
			$__2 = explode("<!-- /" . $it . " -->",$__1[1]);
			$___1 = $__1[0];
			$___2 = $__2[0];
			$___3 = $__2[1];
			$____1 = $___1 . $___3;
			return $____1;
		}
		function addBlockContent($to,$data,$toRemoves = "") {
			$this->thisBlockb = $this->BLOCKS[$to]["p"];
			if ($toRemoves != "") {
				foreach($toRemoves as $rem) {
					$this->thisBlockb = $this->remove($rem,$this->thisBlockb);
				}
			}
			$this->BLOCKS[$to]["block"] .= str_replace($this->BLOCKS[$to]["tags"],$data,$this->thisBlockb);
		}
		function apply() {
			if ($this->list!="") {
				foreach ($this->list as $b) {
					$this->content = str_replace("<!-- " . $this->BLOCKS[$b]["tag"] . " -->",$this->BLOCKS[$b]["block"],$this->content);
				}
			}
			//$this->content = preg_replace("<!-- [A-Z\-]+ -->","",$this->content);
			//$this->content = preg_replace("<!-- /[A-Z\-]+ -->","",$this->content);
		}
		function getContent() {
			return $this->content;
		}
		function embed($tag,$data) {
			$this->content = str_replace($tag,$data,$this->content);
		}
		function embedF($tag,$file) {
			$ff = fopen($file,"r");
			$ret = fread($ff,filesize($file));
			fclose($ff);
			$this->embed($tag,$ret);
		}
		
	}

?>