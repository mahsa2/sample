<?php
	error_reporting(E_ALL);
	ini_set('display_errors', true);
	
	require('init.php');
	
	$db = new DB($dbhost,$dbusername,$dbpassword,$dbname);
	
	if (!$db->open()) {
		die($db->error());
	}
	
	if ($db->queryNoPrams("ALTER TABLE cs_stock_adjustment ADD CONSTRAINT check_value CHECK (Value >= 0)") == ''){
		die($db->error());
	}
	
	
