//Cookie settet and getter and apply to search result
function setCookie() {
	//alert(document.getElementById("searchSuggestion").value);
	var val= document.getElementById("searchSuggestion").value;
	if (val == '') return;
    var d = new Date();
    d.setTime(d.getTime() + (24*60*60*1000*30));
    document.cookie = "searchQuery=" + val + "; " + "expires=" + d.toGMTString();
}

function getCookie(varName) {
    var name = varName + "=";
    var texts = document.cookie.split(';');
    
    for(var i = 0; i < texts.length; i++) {
        var text = texts[i];
        while (text.charAt(0)==' ') text = text.substring(1);
        if (text.indexOf(name) != -1) {
            return [text.substring(name.length, text.length)];
        }
    }
    return "";
}

function updateCookie() {
	var last_search_query = getCookie("searchQuery");
    if (last_search_query != "") {
    	//For debugging purposes you can uncomment the line below
    	//alert("Last Cookie: "+last_search_query);
    	$("#searchSuggestion").autocomplete({source:last_search_query});
    }
}

$(document).ready(function() {
	updateCookie();
    $('#search_form').on('submit', function(e) {
    	setCookie();
        e.preventDefault();
        $.ajax({
            url : $(this).attr('action') || window.location.pathname,
            type: 'GET',
            data: $(this).serialize(),
            success: function (data) {
            	//alert(data);
            	$("#result_toggler").css("display","none");
                $("#result_toggler").html(data);
                $("#result_toggler").show("clip",400);
                //$('#result_toggler').animate({opacity:1, marginTop:-30},'slow');
            },
            error: function (jXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
        updateCookie();
    });
});

/*
// Toggling the result section in search
function showResults() {
	// Stop form from submitting normally
	event.preventDefault();
	// Get some values from elements on the page:
	var $form = $( "#search_form" ),
	term = $form.find( "input[name='search_input']" ).val(),
	url  = $form.attr( "action" );
	// Send the data using post
	var posting = $.post( url, { search_input: term } );
	// Put the results in a div
	posting.done(function( data ) {
		var content = $( data ).find( "#content" );
		$( "#result_toggler" ).fadeIn( "slow");
	});
	//alert("done");
}
*/