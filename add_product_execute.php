<?php
	include("cfg/bootstrap.php");
	
	if (!isset($_POST["product"])) header("Location: add_product.php?add=false");
	
	if ($db->queryArrayParams("INSERT INTO cs_product VALUES(NULL,?,?,?)",
			array('sis'), array(&$_POST["product"], &$_POST["category_id"], &$_POST["product_desc"])) == '') {
		die($db->error());
	}
	
	if (!($res = $db->query("SELECT ID FROM cs_product ORDER BY ID DESC LIMIT 1"))) {
		die($db->error());
	}
	
	$date_time = date('Y-m-d h:i:s');
	foreach ($res as $i) {
		$last_id = $i["ID"];
	}
	if ($db->queryArrayParams("INSERT INTO cs_stock_adjustment VALUES(NULL,?,'Incoming',0,?)",
			array('is'), array(&$last_id, &$date_time )) == '') {
		die($db->error());
	}
	
	header("Location: add_product.php?add=true");
	exit;
?>