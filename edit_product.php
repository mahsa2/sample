<?php
	
	include("cfg/bootstrap.php");
	include("cfg/theme.inc.php");
	
	if (!isset($_GET["eid"])) header("Location:search.php?edit=false");
	
	$T = new Theme("templates/page.html");
	
	$T->embed("[Title]","Edit a Product");
	
	$T->embedF("[PAGE]","templates/main_layout.html");
	$T->embedF("[INPUTBAR]", "templates/edit/bar.html");
	$T->embed("[MESSAGE]", "");
	$T->removeBlock("MIDDLE");
	
	$tmp_get = $_GET["eid"];
	$params_query = array(&$tmp_get);
	$product_stmt = $db->queryArrayParams("SELECT * FROM cs_product WHERE ID=?",
									  array(str_repeat('i', count($params_query))), $params_query);
	$params = array(&$ProductID, &$ProductName, &$ProductCategory, &$ProductDescription);
	$product = $db->fetchAssocWithBind($product_stmt, $params);
	$db->closeStatement($product_stmt);

	if (count($product) == 0) {
		header("Location:search.php?edit:false");
	}
	
	$adjustments_stmt = $db->queryArrayParams("SELECT ID, Adjustment, Value, Timestamp FROM cs_stock_adjustment WHERE PRODUCTID=?",
									  array(str_repeat('i', count($params_query))), $params_query);
	$adjust_params = array(&$StockID, &$StockAdjustment, &$StockValue, &$StockTime);
	$adjusts = $db->fetchAssocWithBind($adjustments_stmt, $adjust_params);
	$db->closeStatement($adjustments_stmt);

	$categories = $db->query("SELECT ID, Name FROM cs_product_category");					
	
	//Setting values in html
	
	$T->embed("[ID]", $product[0][0]);//row is zero because we only expect to get one row from our query for each id. Second 0->ID.
	$T->embed("[PRODUCT]",$product[0][1]);//Name
	$T->embed("[PRODUCT_DESCRIPTION]",$product[0][3]);//Description
	
	$T->newBlock("STOCK",array("[TIMESTAMP]","[STOCK_STATUS]","[STOCK_VID]","[VALUE]"));
	
	foreach($adjusts as $a) {
		//0->"ID",1->"Adjustment, 2->"Value"
		$T->addBlockContent("STOCK", array($a[3], ($a[1] == "Incoming")? "In" : "Out of", $a[0], $a[2]));
	}
	
	$T->newBlock("CATEGORY_OPTION_SELECTED", array("[CATEGORY_ID]", "[CATEGORY]"));
	$T->newBlock("CATEGORY_OPTIONS", array("[CATEGORY_ID]", "[CATEGORY]"));
	
	
	foreach($categories as $c) {
		//0->ID, 1->Name;
		$T->addBlockContent(($c["ID"] == $product[0][2])?"CATEGORY_OPTION_SELECTED":"CATEGORY_OPTIONS", array($c["ID"], $c["Name"]));
	}
	
	$db->closeResult($categories);
	$T->apply();
	echo($T->getContent());			
?>