<?php	
	include("cfg/bootstrap.php");
	include("cfg/theme.inc.php");
	
	$T = new Theme("templates/page.html");
	
	$T->embed("[Title]","Search Products");
	
	$T->embedF("[PAGE]","templates/main_layout.html");
	$T->embedF("[INPUTBAR]", "templates/search/bar.html");
	$T->embed("[MIDDLE]","");
	$products = '';
	
	if (isset($_GET["del"])) {
		if ($_GET["del"] == "true") {
			$T->embed("[MESSAGE]","One Item has been deleted successfully!");
		} else {
			$T->embed("[MESSAGE]","Deleting was unsuccessful!");
		}
	} else {
		$T->embed("[MESSAGE]","");
	}
	
	if (isset($_GET["search_input"])) {
		$respond = new Theme("templates/search/results.html");
		$get_val = '%'.str_replace('%','\%',$_GET["search_input"]).'%';
		$params_query = array(&$get_val, &$get_val, &$get_val, &$get_val);
		
		$products_stmt = $db->queryArrayParams(
								"SELECT PRODUCTS.ID, PRODUCTS.Prod_Name, PRODUCTS.Prod_Desc, 
								PRODUCTS.Name, PRODUCTS.Description,
								Stock.Adjustment, Stock.Value
								FROM cs_stock_adjustment Stock
                                INNER JOIN
                                (SELECT Product.ID, Product.Name AS Prod_Name, 			
                                Product.Description AS Prod_Desc, 
	   							Category.Name, Category.Description
                                FROM cs_product Product
                                INNER JOIN 
                                cs_product_category Category
								ON Product.ProductCategory = Category.ID
                                WHERE (Product.Name LIKE ?
				                OR Product.Description LIKE ? OR 
								Category.Name LIKE ?
				                OR Category.Description LIKE ?)) As PRODUCTS
                                ON Stock.ProductID = PRODUCTS.ID",
								array(str_repeat('s', count($params_query))),
							    $params_query);
		//$params = array(&$vv);
		$params = array(&$ProductID, &$CategoryName, &$CategoryDescription,
						&$ProductName, &$ProductDescription,
						&$Adjustment, &$Value);
		$products = $db->fetchAssocWithBind($products_stmt, $params);
		$db->closeStatement($products_stmt);
		//var_dump($products);
		if (count($products) == 0) {
			$respond->removeBlock("SEARCH");
		} else {
			/* TODO: refactor NORES to NO_RESULT*/
			$respond->removeBlock("NORES");
			$respond->newBlock("SEARCHRESULT",array("[ID]","[PRODUCT_NAME]","[PR_DESCRIPTION]",
					"[CATEGORY]","[CAT_DESCRIPTION]","[LEVEL]"));
			
			$sum = array();
			// row will be each record of the found results from DB and the value order can be found out from above.
			foreach ($products as $row) {
				//row[0] = product_id, Checking the Adjustment (row[5]) to whether subtract the value (row[6]) or add it
				if (array_key_exists($row[0], $sum)) {
					$sum[$row[0]] += ($row[5] != "Outgoing")?$row[6]:-$row[6];
				} else {
					$sum[$row[0]] = ($row[5] != "Outgoing")?$row[6]:-$row[6];
				}
			}
			
			$existing_products = array();
			foreach($products as $row) {
				if (!in_array($row[0], $existing_products)) {
					$respond->addBlockContent("SEARCHRESULT", array($row[0],$row[1],$row[2],$row[3],$row[4],$sum[$row[0]]));
					array_push($existing_products, $row[0]);
				}
			}
			$total = 0;
			foreach($sum as $s=>$v) {
				$total += $v;
			}
			$respond->embed("[TOTAL]",array_sum($sum));
		}
		$respond->apply();
		echo $respond->getContent();
	} else {
		$T->apply();
		echo $T->getContent();
	}	
?>