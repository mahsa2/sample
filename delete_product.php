<?php
	include("cfg/bootstrap.php");
	if (!isset($_GET["id"])) header("Location: search.php?del=false");
	
	$tmp_get = $_GET["id"];
	$params_query = array(&$tmp_get);
	if ($db->queryArrayParams("DELETE FROM cs_product WHERE id=?",
			array(str_repeat('i', count($params_query))), $params_query) == '') {
		die($db->error());
	}
	
	if ($db->queryArrayParams("DELETE FROM cs_stock_adjustment WHERE ProductID=?",
			array(str_repeat('i', count($params_query))), $params_query) == '') {
		die($db->error());
	}
	
	header("Location: search.php?del=true");
	exit;
?>
	