<?php
	
	include("cfg/bootstrap.php");
	include("cfg/theme.inc.php");
	
	$T = new Theme("templates/page.html");
	
	$T->embed("[Title]","Add a Product");
	
	$T->embedF("[PAGE]","templates/main_layout.html");
	$T->embedF("[INPUTBAR]", "templates/add/bar.html");
	$T->removeBlock("MIDDLE");
	
	if (isset($_GET["add"])) {
		if ($_GET["add"] == "true") {
			$T->embed("[MESSAGE]","One Item has been added successfully!");
		} else {
			$T->embed("[MESSAGE]","Adding was unsuccessful!");
		}
	} else {
		$T->embed("[MESSAGE]","");
	}
	
	$categories = $db->query("SELECT ID, Name FROM cs_product_category");
	
	$T->newBlock("CATEGORY_OPTIONS", array("[CATEGORY_ID]", "[CATEGORY]"));
	
	foreach($categories as $c) {
		$T->addBlockContent("CATEGORY_OPTIONS", array($c["ID"], $c["Name"]));
	}
	
	$db->closeResult($categories);
	
	$T->apply();
	echo($T->getContent());			
?>